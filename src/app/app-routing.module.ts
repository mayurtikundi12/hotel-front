import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RoutingGuardService } from './routing/routing-guard.service'
import { HotelsComponent } from './hotels/hotels/hotels.component';
import { HotelsDetailsComponent } from './hotels/hotels-details/hotels-details.component';
import { EventsComponent } from './hotels/events/events.component';
import { SpecialsComponent } from './hotels/specials/specials.component';
import { BookhotelComponent } from './hotels/bookhotel/bookhotel.component';
import { DashboardComponent } from './users/dashboard/dashboard.component';
import { CarousalComponent } from './home/carousal/carousal.component';
import { TopRatedHotelsComponent } from './home/top-rated-hotels/top-rated-hotels.component';
import {RegisterformComponent} from './users/registerform/registerform.component'
import  { LoginformComponent} from'./users/loginform/loginform.component'
import {AddhotelComponent} from './hotels/addhotel/addhotel.component'
import {RoutingHotelsService} from './routing/routing-hotels.service'
import {PageNotFoundComponent} from './home/page-not-found/page-not-found.component'

const routes: Routes = [
  {path:"hotels",component:HotelsComponent, canActivate:[RoutingGuardService] },
  {path:"home/hotelsdetails/:id",component:HotelsDetailsComponent, canActivate:[RoutingGuardService] },
  {path:"hotels/events",component:EventsComponent , canActivate:[RoutingGuardService]},
  {path:"hotels/special",component:SpecialsComponent, canActivate:[RoutingGuardService] },
  {path:"hotels/bookhotel",component:BookhotelComponent, canActivate:[RoutingGuardService] },
  {path:"topratedhotels",component:TopRatedHotelsComponent, canActivate:[RoutingGuardService] },
  {path:"user/dashboard",component:DashboardComponent , canActivate:[RoutingGuardService]},
  {path:"hotels/add",component:AddhotelComponent , canActivate:[RoutingGuardService,RoutingHotelsService]},
  {path:"home",component:CarousalComponent, canActivate:[RoutingGuardService]},
  {path:"login",component:LoginformComponent},
  {path:"register",component:RegisterformComponent},
  {path:'',redirectTo:"home",pathMatch:'full'},
  {path:'**',component:PageNotFoundComponent}

];

@NgModule({
  imports: [CommonModule,RouterModule.forRoot(routes),FormsModule],
  declarations:[HotelsComponent,HotelsDetailsComponent,
    EventsComponent,SpecialsComponent,
    BookhotelComponent,DashboardComponent,
    CarousalComponent,TopRatedHotelsComponent ,
    LoginformComponent,
    RegisterformComponent
  ] ,
  exports: [RouterModule]
})
export class AppRoutingModule { }
