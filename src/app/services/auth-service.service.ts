import { Injectable } from '@angular/core';
import * as CONFIG from '../config/app.properties' ;
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private LoginUrl =`http://localhost:3050/user/login`
  private RegisterUrl =`http://${CONFIG.ENV.serverIp}:${CONFIG.ENV.port}/user/register`
  
  constructor(private http:HttpClient) { }
  userLogin(user){
    return this.http.post<any>(this.LoginUrl,user);

  }

  userRegistration(user){
    return this.http.post<any>(this.RegisterUrl,user);
  }

  isLoggedIn():boolean{
    if(localStorage.getItem('token')){
      return true;
    }else{
      return false ;
    }
  }
  getToken(){  
    return localStorage.getItem('token');    
  }
  getUserType(){
    return localStorage.getItem('role') ;
  }
}
