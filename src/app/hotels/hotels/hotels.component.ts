import { Component, OnInit } from '@angular/core';
import { HotelsService } from '../hotels.service'
@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {

  hotels:Object;  
  hotelname:string;
  hotelsArray:string;
  availablehotels=[];
  hotelFoundFlag=false ;
  constructor(private hotelSrv:HotelsService) { }

  ngOnInit() {
    this.hotelSrv.getAllHotels().subscribe(response=>{      
      this.hotels=response['payload'];
   },error=>{
     console.log(error);
   });
  }
  getHotelsByName(){
    console.log(this.hotelname);
    for (let key in this.hotels) {
     let flag= this.hotels[key].name.includes(this.hotelname) ;
     if(flag){
       this.availablehotels.push(this.hotels[key].name);
       this.hotelFoundFlag=true ;
     }else{
       console.log("no matching word");
       this.hotelFoundFlag=false ;
     }
    }
    if(this.hotelFoundFlag){
      this.availablehotels.push("sorry no match found");
    }

  }

}
