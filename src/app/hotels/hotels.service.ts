import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class HotelsService {
  private hotelsUrl = 'http://localhost:3050/hotels'
  private hotelUrl = 'http://localhost:3050/hotel'
  private allHotelUrl = 'http://localhost:3050/allhotels'

  constructor(private http:HttpClient) {
   }

   getHotels(){
    return this.http.get(this.hotelsUrl) ;
   }
   getAllHotels(){
    return this.http.get(this.allHotelUrl) ;

   }
   getHotelsByFilter(offset:number,count:number){
    return this.http.get(this.hotelsUrl+"?offset="+offset+"&count="+count) ;
   }
   getOneHotel(hotelId){
     return this.http.get(this.hotelUrl+"/"+hotelId)
   }

   addHotel(hotel){
     return this.http.post<any>(this.hotelUrl,hotel)
   }
}
