import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router'
import { HotelsService } from '../hotels.service';
@Component({
  selector: 'app-hotels-details',
  templateUrl: './hotels-details.component.html',
  styleUrls: ['./hotels-details.component.css']
})
export class HotelsDetailsComponent implements OnInit {

    hotelId;
    hotel;
  constructor(private routes:ActivatedRoute , private hotelSrv:HotelsService) { }

  ngOnInit() {
    this.routes.paramMap.subscribe(params=>{
      console.log(params.get('id'));
      this.hotelId = params.get('id')
    });
    this.hotelSrv.getOneHotel(this.hotelId).subscribe(
      res=>{
        console.log(res);
        this.hotel=res['payload'] ;
      },
      error=>{
        console.log(error);
      }
    )
  }



}
