import { Component, OnInit } from '@angular/core';
import {HotelsService } from '../hotels.service'
@Component({
  selector: 'app-addhotel',
  templateUrl: './addhotel.component.html',
  styleUrls: ['./addhotel.component.css']
})
export class AddhotelComponent implements OnInit {

  hotel={
    name:'',
    stars:'',
    description:''
  }

  constructor(private hotelSrv:HotelsService) { }

  ngOnInit() {}
  addHotel(){
    return this.hotelSrv.addHotel(this.hotel).subscribe(response=>{
      console.log(response);
    },error=>{
      console.log(error);
    }) ;
  }
}
