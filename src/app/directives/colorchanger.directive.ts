import { Directive ,HostListener,ElementRef, Renderer} from '@angular/core';

@Directive({
  selector: '[appColorchanger]'
})
export class ColorchangerDirective {

  constructor(private elem:ElementRef , private renderer:Renderer,) { 
    this.changeColor();
  }

  changeColor(){
    this.renderer.setElementStyle(this.elem.nativeElement,'color','red')
  }
  @HostListener('click')customclick(){
    this.newcolor()
  }

  newcolor(){
    this.renderer.setElementStyle(this.elem.nativeElement,'color','blue')
  }

}
