import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router' ;
import { AuthService } from '../services/auth-service.service'

@Injectable({
  providedIn: 'root'
})
export class RoutingHotelsService implements CanActivate{

  constructor(private authSrv:AuthService, private _route:Router) { }
  canActivate(){
    if(this.authSrv.isLoggedIn()){
      if(this.authSrv.getUserType() =='hotelAdmin'){
        return true;
      }else{
        this._route.navigate(['/login'])
        return false ;
      }
    }
  }
}
