import { TestBed } from '@angular/core/testing';

import { RoutingHotelsService } from './routing-hotels.service';

describe('RoutingHotelsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoutingHotelsService = TestBed.get(RoutingHotelsService);
    expect(service).toBeTruthy();
  });
});
