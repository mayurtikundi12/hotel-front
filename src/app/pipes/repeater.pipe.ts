import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'repeater'
})
export class RepeaterPipe implements PipeTransform {
  finalval ;
  transform(value: string, count:number): string {
    this.finalval = value.repeat(count)
    return this.finalval;
  }

}
