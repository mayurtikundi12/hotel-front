import { Component, OnInit } from '@angular/core';
import { HotelsService } from '../../hotels/hotels.service'
@Component({
  selector: 'app-top-rated-hotels',
  templateUrl: './top-rated-hotels.component.html',
  styleUrls: ['./top-rated-hotels.component.css']
})
export class TopRatedHotelsComponent implements OnInit {
  hotels ;
  private offset:number=0;
  private count:number=6 ;
  constructor(private hotelSrv:HotelsService) {

   }

  ngOnInit() {
    this.hotelSrv.getHotels().subscribe((response)=>{
      console.log(response);
      this.hotels=response['payload'] ;
    },(err)=>{
      console.log(err);
      
    })
  }
  getHotelsByService(offset:number,count:number){
    this.offset=offset ;
    this.count=count ;
    this.hotelSrv.getHotelsByFilter(offset,count)
    .subscribe((response)=>{
      console.log(response);
      this.hotels=response ;
    },(err)=>{
      console.log(err);
      
    })
  }

}
