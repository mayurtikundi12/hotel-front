import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth-service.service';
import {Router} from '@angular/router' ;
@Component({
  selector: 'app-lowerheader',
  templateUrl: './lowerheader.component.html',
  styleUrls: ['./lowerheader.component.css']
})
export class LowerheaderComponent implements OnInit {
  isAuth:boolean ;
  constructor(private authSrv:AuthService,private route:Router) {

  }

  ngOnInit() {
    if(this.authSrv.isLoggedIn()){
      this.isAuth==true ;
    }else{
      this.isAuth==false ;
    }
  }
  removeToken(){
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    this.route.navigate(['/login'])
  }

}
