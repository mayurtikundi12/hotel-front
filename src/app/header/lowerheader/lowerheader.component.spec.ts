import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LowerheaderComponent } from './lowerheader.component';

describe('LowerheaderComponent', () => {
  let component: LowerheaderComponent;
  let fixture: ComponentFixture<LowerheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LowerheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LowerheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
