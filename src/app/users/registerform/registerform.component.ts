import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth-service.service'
import {Router } from '@angular/router';

@Component({
  selector: 'app-registerform',
  templateUrl: './registerform.component.html',
  styleUrls: ['./registerform.component.css']
})

export class RegisterformComponent implements OnInit {
    registerData={
      name:'',
      email:'',
      password:'',
      phone:''
    }
  constructor(private authSrv:AuthService,private router:Router) { }

  ngOnInit() {
  }
  getData(){
      this.authSrv.userRegistration(this.registerData).subscribe(response=>{
      localStorage.setItem('token',response.token);
      localStorage.setItem('token',response.payload.token);
        this.router.navigate(['/hotels'])
      },error=>{
        console.log(error);      
      })
  }

}
