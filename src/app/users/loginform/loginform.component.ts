import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth-service.service';
import {Router } from '@angular/router';
@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {
  user={
    email:'',
    password:''
  }
  constructor(private authSrv:AuthService,private router:Router) { }

  ngOnInit() {
  }
  authorizeUser(){
    this.authSrv.userLogin(this.user).subscribe(response=>{
      localStorage.setItem('token',response.token);
      localStorage.setItem('role',response.payload.role);
      this.router.navigate(['/hotels']);
    },error=>{
      console.log(error);
    })
  }
}
