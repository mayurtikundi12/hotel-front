import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LowerfooterComponent } from './lowerfooter.component';

describe('LowerfooterComponent', () => {
  let component: LowerfooterComponent;
  let fixture: ComponentFixture<LowerfooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LowerfooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LowerfooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
