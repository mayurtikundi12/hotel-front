import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RoutingGuardService} from './routing/routing-guard.service'
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RoutingHotelsService } from './routing/routing-hotels.service'

import { UpperheaderComponent } from './header/upperheader/upperheader.component';
import { LowerheaderComponent } from './header/lowerheader/lowerheader.component';
import { LowerfooterComponent } from './footer/lowerfooter/lowerfooter.component';
import { UpperfooterComponent } from './footer/upperfooter/upperfooter.component';
import { AddhotelComponent } from './hotels/addhotel/addhotel.component';
import { AuthService } from './services/auth-service.service';
import { HotelsService } from './hotels/hotels.service';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { PageNotFoundComponent } from './home/page-not-found/page-not-found.component';
import { ColorchangerDirective } from './directives/colorchanger.directive';
import { RepeaterPipe } from './pipes/repeater.pipe';
import { SplitterPipe } from './pipes/splitter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    UpperheaderComponent,
    LowerheaderComponent,
    LowerfooterComponent,
    UpperfooterComponent,
    AddhotelComponent,
    PageNotFoundComponent,
    ColorchangerDirective,
    RepeaterPipe,
    SplitterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // CommonModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [AuthService,HotelsService,
    RoutingGuardService,
    RoutingHotelsService,
  {
    provide:HTTP_INTERCEPTORS,
    useClass:TokenInterceptorService,
    multi:true 
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
